const fs = require('fs');

// instantiate object to export from this module for testing purposes
const enlighten = {};

// function to parse through text file and extract relevant information to run the findCoords algorithm with
enlighten.parseFile = (textFile) => {
  // read text file and then store each line as an element in an array called textByLine
  const text = fs.readFileSync(textFile, 'utf-8');
  const textByLine = text.split('\n');
  const tempGrid = [];
  // extract and remove the first line from textByLine array and get the first number which is the number of rows in our grid
  const numberOfRows = parseInt(textByLine.splice(0, 1).toString().charAt(0));
  // set wordsToBeFound to be an array of the elements found after the letter rows in the text file
  enlighten.wordsToBeFound = textByLine.slice(numberOfRows);
  // console.log(enlighten.wordsToBeFound)
  // extract and set 'unformattedGrid' to be and array of the first 5 elements in the textByLine array
  const unformattedGrid = textByLine.splice(0, numberOfRows + 1);
  // iterate through unformattedGrid and push each element into tempGrid so that each letter is its own string within respective array
  for (let i = 0; i < unformattedGrid.length - 1; i += 1) {
    tempGrid.push(unformattedGrid[i].split(' '));
  }
  enlighten.gridOfLetters = tempGrid;
};

// execute parseFile function with text file you wish to run
enlighten.parseFile('input.txt');

// represented the board in a 2D array
// created a function to take in params of the generated grid array and each word to be found
enlighten.findCoords = (grid, word) => {
  // create an array and store coordinates of possible movement from current location on grid
  const directions = [
    [1, 0],
    [-1, 0],
    [0, 1],
    [0, -1],
    [1, 1],
    [-1, 1],
    [1, -1],
    [-1, -1],
  ];

  const dfs = (row, col) => {
    // instantiate an array to [-1, -1] that will update and store coordinates when desired letter is found on grid
    const results = [-1, -1];
    // helper recursive function
    const helper = (r, c, index = 0) => {
      // base case to check if current coordinate is it of the grids boundaries
      if (r < 0 || r >= grid.length || c < 0 || c >= grid[0].length) return;
      // base case to make sure you do not return to a previously checked coordinate
      if (grid[r][c] === ' ') return;
      // base case to return out if the current letter on the grid does not match the current letter in the wordToBeFound you are looking for
      if (word[index] !== grid[r][c]) return;
      // base case to store coordinates if the wordToBeFound has been located on grid
      if (index === word.length - 1 && results[0] === -1) {
        results[0] = r;
        results[1] = c;
        return;
      }
      // create a variable and store current letter of the location you are at on the grid
      const temp = grid[r][c];
      // then set this stored letter to an empty space in order to indicate that you have visited this space
      grid[r][c] = ' ';
      // iterate through the directions array and pass in every possible coordinate direction of movement from current letter spot to our helper function
      for (const [rr, cc] of directions) {
        const newRow = rr + r;
        const newCol = cc + c;
        helper(newRow, newCol, index + 1);
      }
      // backtrack and reset the letter that was being examined to the original value that was stored in 'temp'
      grid[r][c] = temp;
    };
    // execute helper function with search starting coordinates
    helper(row, col);
    // return results coords
    return results;
  };

  // iterate through entire grid, rows and columns
  for (let r = 0; r < grid.length; r += 1) {
    for (let c = 0; c < grid[0].length; c += 1) {
      // if the current letter at our current coordinate of the grid is equal to the first letter in the current wordToBeFound, run our recursive dfs function with those coordinates
      // if the word is found the recursive function will return result coordinates
      if (grid[r][c] === word[0]) {
        const resultCoords = dfs(r, c);
        // check to see that our results coordinates are not invalid (ie. [-1, -1])
        if (resultCoords[0] !== -1) {
          // if the coordinates are valid, destructure out our results coordinates and log them to the console
          const [startResult, endResult] = resultCoords;
          console.log(`${word} ${r}:${c} ${startResult}:${endResult}`);
          return;
        }
      }
    }
  }
};

// iterate through the words to be found array and execute the findCoords function on each word using the wordSearch grid
enlighten.execute = (wordSearch, words) => {
  for (let i = 0; i < words.length; i += 1) {
    (enlighten.findCoords(wordSearch, words[i]));
  }
};

// call the execute function with our data sets, gridOfLetters and wordsToBeFound
enlighten.execute(enlighten.gridOfLetters, enlighten.wordsToBeFound);

module.exports = enlighten;
