const enlighten = require('../enlighten.js');

global.console = {
  warn: jest.fn(),
  log: jest.fn(),
};

describe('enlighten.parseFile() test with test.txt', () => {
  // sample input text file
  enlighten.parseFile('test.txt');
  it('should return expected values with the test.txt param', () => {
    expect(enlighten.wordsToBeFound).toEqual(['ABC', 'AIE']);
    expect(enlighten.gridOfLetters).toEqual([
      ['A', 'B', 'C'],
      ['D', 'E', 'F'],
      ['G', 'H', 'I'],
    ]);
  });
});

describe('enlighten.findCoords() test', () => {
  it('should print correct values to the console when function is called on the same grid with each word in the array', () => {
    enlighten.findCoords(enlighten.gridOfLetters, enlighten.wordsToBeFound[0]);
    expect(global.console.log).toHaveBeenCalledWith('ABC 0:0 0:2');
    enlighten.findCoords(enlighten.gridOfLetters, enlighten.wordsToBeFound[1]);
  });
});

describe('enlighten.execute() test', () => {
  it('should print correct values to the console when function is called with gridOfLetters and wordsToBeFound', () => {
    enlighten.execute(enlighten.gridOfLetters, enlighten.wordsToBeFound);
    expect(global.console.log).toHaveBeenCalledWith('ABC 0:0 0:2');
  });
});
