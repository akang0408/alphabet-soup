const enlighten = require('../enlighten.js');

global.console = {
  warn: jest.fn(),
  log: jest.fn(),
};

describe('enlighten.parseFile() test with test2.txt', () => {
  // text file that has a board that has unequal dimensions and 2 words that share the same first letter
  enlighten.parseFile('test2.txt');
  it('should return expected values with the test.txt param', () => {
    expect(enlighten.wordsToBeFound).toEqual(['ENLIGHTEN', 'HAT', 'HOT']);
    expect(enlighten.gridOfLetters).toEqual([
      [
        'E', 'N', 'L',
        'I', 'G', 'H',
        'T', 'E', 'N',
      ],
      [
        'E', 'P', 'P',
        'P', 'H', 'A',
        'O', 'J', 'J',
      ],
      [
        'H', 'H', 'H', 'A',
        'D', 'T', 'J', 'T',
        'J', '',
      ],
    ]);
  });
});

describe('enlighten.findCoords() test', () => {
  it('should print correct values to the console when function is called on the same grid with each word in the array', () => {
    enlighten.findCoords(enlighten.gridOfLetters, enlighten.wordsToBeFound[0]);
    expect(global.console.log).toHaveBeenCalledWith('ENLIGHTEN 0:0 0:8');
    enlighten.findCoords(enlighten.gridOfLetters, enlighten.wordsToBeFound[1]);
    expect(global.console.log).toHaveBeenCalledWith('HAT 0:5 2:5');
    enlighten.findCoords(enlighten.gridOfLetters, enlighten.wordsToBeFound[2]);
    expect(global.console.log).toHaveBeenCalledWith('HOT 0:5 0:6');
  });
});

describe('enlighten.execute() test', () => {
  it('should print correct values to the console when function is called with gridOfLetters and wordsToBeFound', () => {
    enlighten.execute(enlighten.gridOfLetters, enlighten.wordsToBeFound);
    expect(global.console.log).toHaveBeenCalledWith('ENLIGHTEN 0:0 0:8');
    expect(global.console.log).toHaveBeenCalledWith('HAT 0:5 2:5');
    expect(global.console.log).toHaveBeenCalledWith('HOT 0:5 0:6');
  });
});
