const enlighten = require('../enlighten.js');

global.console = {
  warn: jest.fn(),
  log: jest.fn(),
};

describe('testing enlighten functionality', () => {
  describe('enlighten.parseFile() with input.text', () => {
    // sample input text file
    enlighten.parseFile('input.txt');
    it('should return expected values with the input.txt param', () => {
      expect(enlighten.wordsToBeFound).toEqual(['HELLO', 'GOOD', 'BYE']);
      expect(enlighten.gridOfLetters).toEqual([
        ['H', 'A', 'S', 'D', 'F'],
        ['G', 'E', 'Y', 'B', 'H'],
        ['J', 'K', 'L', 'Z', 'X'],
        ['C', 'V', 'B', 'L', 'N'],
        ['G', 'O', 'O', 'D', 'O'],
      ]);
    });
  });

  describe('enlighten.findCoords() test', () => {
    it('should print correct values to the console when function is called on the same grid with each word in the array', () => {
      enlighten.findCoords(enlighten.gridOfLetters, enlighten.wordsToBeFound[0]);
      expect(global.console.log).toHaveBeenCalledWith('HELLO 0:0 4:4');
      enlighten.findCoords(enlighten.gridOfLetters, enlighten.wordsToBeFound[1]);
      expect(global.console.log).toHaveBeenCalledWith('GOOD 4:0 4:3');
      enlighten.findCoords(enlighten.gridOfLetters, enlighten.wordsToBeFound[2]);
      expect(global.console.log).toHaveBeenCalledWith('BYE 1:3 1:1');
    });
  });

  describe('enlighten.execute() test', () => {
    it('should print correct values to the console when function is called with gridOfLetters and wordsToBeFound', () => {
      enlighten.execute(enlighten.gridOfLetters, enlighten.wordsToBeFound);
      expect(global.console.log).toHaveBeenCalledWith('HELLO 0:0 4:4');
      expect(global.console.log).toHaveBeenCalledWith('GOOD 4:0 4:3');
      expect(global.console.log).toHaveBeenCalledWith('BYE 1:3 1:1');
    });
  });
});
